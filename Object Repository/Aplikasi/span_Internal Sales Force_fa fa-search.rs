<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Internal Sales Force_fa fa-search</name>
   <tag></tag>
   <elementGuidId>10c8d463-73e0-4561-8155-bfdc4001c99b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='btn-ide-aplikasi-int-sales-empl']/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;btn-ide-aplikasi-int-sales-empl&quot;)/span[@class=&quot;fa fa-search&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-search</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;btn-ide-aplikasi-int-sales-empl&quot;)/span[@class=&quot;fa fa-search&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='btn-ide-aplikasi-int-sales-empl']/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form[2]/div/div[3]/div[2]/div/div[2]/div/span/button/span</value>
   </webElementXpaths>
</WebElementEntity>
