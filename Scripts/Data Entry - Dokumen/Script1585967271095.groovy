import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Login/a_Dokumen'))

if (findTestData('Aplikasi').getValue(11, 1) == '04 - AUTO DEBET') {
    //jika autodebet
    if ((findTestData('Objek Pembiayaan').getValue(1, 1) == '003') || (findTestData('Objek Pembiayaan').getValue(1, 1) == 
    '004')) {
        //40	SURAT KUASA PENDEBETAN
        WebUI.click(findTestObject('Object Repository/Dokumen/Autodebet/input_Received_radio-doc13'))

        WebUI.setText(findTestObject('Object Repository/Dokumen/Autodebet/input_Received_inp-tanggal-terima-document13'), 
            findTestData('Dokumen').getValue(4, 1))

        WebUI.click(findTestObject('Dokumen/Autodebet/save_dok_row_13'))

        WebUI.uploadFile(findTestObject('Dokumen/Upload File'), findTestData('Dokumen').getValue(1, 1))

        WebUI.click(findTestObject('Dokumen/button_Upload Dokumen'))

        WebUI.delay(1)

        WebUI.click(findTestObject('Object Repository/Dokumen/button_OK'))

        //21 - KTP/RESI/KTP SEMENTARA PEMOHON
        WebUI.click(findTestObject('Object Repository/Dokumen/Autodebet/input_Received_radio-doc7'))

        WebUI.setText(findTestObject('Object Repository/Dokumen/Autodebet/input_Received_inp-tanggal-terima-document7'), 
            findTestData('Dokumen').getValue(4, 1))

        WebUI.click(findTestObject('Dokumen/Autodebet/save_dok_row_7'))

        WebUI.uploadFile(findTestObject('Dokumen/Upload File'), findTestData('Dokumen').getValue(1, 1))

        WebUI.click(findTestObject('Dokumen/button_Upload Dokumen'))

        WebUI.delay(1)

        WebUI.click(findTestObject('Object Repository/Dokumen/button_OK' //40	SURAT KUASA PENDEBETAN
                ) //21 - KTP/RESI/KTP SEMENTARA PEMOHON
            )
    } else {
        WebUI.click(findTestObject('Object Repository/Dokumen/Autodebet/input_Received_radio-doc10'))

        WebUI.setText(findTestObject('Object Repository/Dokumen/Autodebet/input_Received_inp-tanggal-terima-document10'), 
            findTestData('Dokumen').getValue(4, 1))

        WebUI.click(findTestObject('Dokumen/Autodebet/save_dok_row_10'))

        WebUI.uploadFile(findTestObject('Dokumen/Upload File'), findTestData('Dokumen').getValue(1, 1))

        WebUI.click(findTestObject('Dokumen/button_Upload Dokumen'))

        WebUI.delay(1)

        WebUI.click(findTestObject('Object Repository/Dokumen/button_OK'))

        WebUI.click(findTestObject('Object Repository/Dokumen/Autodebet/input_Received_radio-doc6'))

        WebUI.setText(findTestObject('Object Repository/Dokumen/Autodebet/input_Received_inp-tanggal-terima-document6'), 
            findTestData('Dokumen').getValue(4, 1))

        WebUI.click(findTestObject('Dokumen/Autodebet/save_dok_row_6'))

        WebUI.uploadFile(findTestObject('Dokumen/Upload File'), findTestData('Dokumen').getValue(1, 1))

        WebUI.click(findTestObject('Dokumen/button_Upload Dokumen'))

        WebUI.delay(1)

        WebUI.click(findTestObject('Object Repository/Dokumen/button_OK'))
    }
    
    //18	KK/AKTE NIKAH/AKTE KELAHIRAN
    WebUI.click(findTestObject('Object Repository/Dokumen/Autodebet/input_Received_radio-doc5'))

    WebUI.setText(findTestObject('Object Repository/Dokumen/Autodebet/input_Received_inp-tanggal-terima-document5'), findTestData(
            'Dokumen').getValue(4, 1))

    WebUI.click(findTestObject('Dokumen/Autodebet/save_dok_row_5'))

    WebUI.uploadFile(findTestObject('Dokumen/Upload File'), findTestData('Dokumen').getValue(1, 1))

    WebUI.click(findTestObject('Dokumen/button_Upload Dokumen'))

    WebUI.delay(1)

    WebUI.click(findTestObject('Object Repository/Dokumen/button_OK'))

    //11 - COPY KTP PEMILIK REKENING ROW 4
    WebUI.click(findTestObject('Dokumen/Autodebet/input_Received_radio-doc4'))

    WebUI.setText(findTestObject('Dokumen/Autodebet/input_Received_inp-tanggal-terima-document4'), findTestData('Dokumen').getValue(
            4, 1))

    WebUI.click(findTestObject('Dokumen/Autodebet/save_dok_row_4'))

    WebUI.uploadFile(findTestObject('Dokumen/Upload File'), findTestData('Dokumen').getValue(1, 1))

    WebUI.click(findTestObject('Dokumen/button_Upload Dokumen'))

    WebUI.delay(1)

    WebUI.click(findTestObject('Object Repository/Dokumen/button_OK'))

    //9 - COPY KTP A.N PEMOHON ROW 3
    WebUI.click(findTestObject('Dokumen/Autodebet/input_Received_radio-doc3'))

    WebUI.setText(findTestObject('Dokumen/Autodebet/input_Received_inp-tanggal-terima-document3'), findTestData('Dokumen').getValue(
            4, 1))

    WebUI.click(findTestObject('Dokumen/Autodebet/save_dok_row_3'))

    WebUI.uploadFile(findTestObject('Dokumen/Upload File'), findTestData('Dokumen').getValue(1, 1))

    WebUI.click(findTestObject('Dokumen/button_Upload Dokumen'))

    WebUI.delay(1)

    WebUI.click(findTestObject('Object Repository/Dokumen/button_OK'))

    // 7 - 	COPY BUKU TABUNGAN ROW 2
    WebUI.click(findTestObject('Object Repository/Dokumen/Autodebet/input_Received_radio-doc2'))

    WebUI.setText(findTestObject('Object Repository/Dokumen/Autodebet/input_Received_inp-tanggal-terima-document2'), findTestData(
            'Dokumen').getValue(4, 1))

    WebUI.click(findTestObject('Dokumen/Autodebet/save_dok_row_2'))

    WebUI.uploadFile(findTestObject('Dokumen/Upload File'), findTestData('Dokumen').getValue(1, 1))

    WebUI.click(findTestObject('Dokumen/button_Upload Dokumen'))

    WebUI.delay(1)

    WebUI.click(findTestObject('Object Repository/Dokumen/button_OK' //jika bukan autodebet
            ))
} else {
    WebUI.click(findTestObject('Dokumen/input_Received_radio-doc7'))

    WebUI.setText(findTestObject('Dokumen/input_Received_inp-tanggal-terima-document7'), findTestData('Dokumen').getValue(
            4, 1))

    WebUI.click(findTestObject('Dokumen/click_save_row_7'))

    WebUI.uploadFile(findTestObject('Dokumen/Upload File'), findTestData('Dokumen').getValue(1, 1))

    WebUI.click(findTestObject('Dokumen/button_Upload Dokumen'))

    WebUI.delay(1)

    WebUI.click(findTestObject('Object Repository/Dokumen/button_OK'))

    WebUI.click(findTestObject('Dokumen/input_Received_radio-doc3'))

    WebUI.setText(findTestObject('Dokumen/input_Received_inp-tanggal-terima-document3'), findTestData('Dokumen').getValue(
            4, 1))

    WebUI.click(findTestObject('Dokumen/click_save_row_3'))

    WebUI.uploadFile(findTestObject('Dokumen/Upload File'), findTestData('Dokumen').getValue(2, 1))

    WebUI.click(findTestObject('Object Repository/Dokumen/button_Upload Dokumen'))

    WebUI.delay(1)

    WebUI.click(findTestObject('Object Repository/Dokumen/button_OK'))

    WebUI.click(findTestObject('Dokumen/input_Received_radio-doc2'))

    WebUI.setText(findTestObject('Dokumen/input_Received_inp-tanggal-terima-document2'), findTestData('Dokumen').getValue(
            4, 1))

    WebUI.click(findTestObject('Dokumen/click_save_row_2'))

    WebUI.uploadFile(findTestObject('Dokumen/Upload File'), findTestData('Dokumen').getValue(2, 1))

    WebUI.click(findTestObject('Object Repository/Dokumen/button_Upload Dokumen'))

    WebUI.delay(1)

    WebUI.click(findTestObject('Object Repository/Dokumen/button_OK'))
}

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/Dokumen/button_Konfirmasi'))

WebUI.delay(3)

WebUI.click(findTestObject('Dokumen/button_Ya'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Dokumen/button_OK'))

WebUI.closeBrowser()
