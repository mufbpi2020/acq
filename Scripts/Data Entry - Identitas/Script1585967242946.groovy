import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords

int slow = 1

//Panggil Navigate URL
WebUI.callTestCase(findTestCase('Go To Link'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('ACQ - Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Login/a_DATA ENTRY'))

WebUI.delay(slow)

WebUI.click(findTestObject('Login/button_Create New Order'))

WebUI.selectOptionByValue(findTestObject('Login/select_--SILAHKAN PILIH--PER-PERSONALCOM-COMPANY'), findTestData('Identitas').getValue(
        1, 1), true)

if (findTestData('Identitas').getValue(1, 1) == 'PER') {
    WebUI.setText(findTestObject('Login/input_Nomor KTP_inp_ktp'), findTestData('Identitas').getValue(2, 1))

    WebUI.setText(findTestObject('Login/input_Nama_inp-dedup-nama'), findTestData('Identitas').getValue(3, 1))

    WebUI.setText(findTestObject('Login/input_Tanggal Lahir _inp-dedup-tanggal'), findTestData('Identitas').getValue(4, 
            1))

    WebUI.setText(findTestObject('Login/input_Nama Gadis Ibu Kandung _inp-dedup-ibu'), findTestData('Identitas').getValue(
            5, 1))
} else {
    WebUI.setText(findTestObject('Object Repository/Login/input_Nomor NPWP_inp_npwp'), findTestData('Identitas').getValue(
            2, 2))

    WebUI.setText(findTestObject('Object Repository/Login/input_Nama Perusahaan_nama_com'), findTestData('Identitas').getValue(
            3, 2))

    WebUI.setText(findTestObject('Object Repository/Login/input_Tanggal Berdiri Perusahaan _inp-dedup_31f715'), findTestData(
            'Identitas').getValue(4, 2))
}

WebUI.click(findTestObject('Login/button_Detection Duplication'))

WebUI.delay(slow)

WebUI.click(findTestObject('Login/button_To Data Entry'))

WebUI.delay(slow)

if (findTestData('Identitas').getValue(1, 1) == 'PER') {
    WebUI.selectOptionByLabel(findTestObject('Login/select_--SILAHKAN PILIH--01 - KTP03 - PASSP_acec17'), findTestData('Identitas').getValue(
            6, 1), false)

    WebUI.setText(findTestObject('Login/input_Tanggal Identitas_inp-issued-card'), findTestData('Identitas').getValue(7, 
            1))

    WebUI.setText(findTestObject('Login/input_sd_inp-expired-card'), findTestData('Identitas').getValue(8, 1))

    WebUI.selectOptionByLabel(findTestObject('Object Repository/Login/select_--SILAHKAN PILIH--00 - TANPA GELAR01_d7f1ce'), 
        findTestData('Identitas').getValue(9, 1), true)

    WebUI.selectOptionByLabel(findTestObject('Object Repository/Login/select_--SILAHKAN PILIH--01 - KAWIN02 - SIN_056651'), 
        findTestData('Identitas').getValue(10, 1), true)

    WebUI.setText(findTestObject('Object Repository/Login/input_Nama Lengkap Sesuai Identitas_inp-nam_302bf2'), findTestData(
            'Identitas').getValue(11, 1))

    WebUI.setText(findTestObject('Object Repository/Login/input_Nama Lengkap SID_inp-nama-lengkap-sid'), findTestData('Identitas').getValue(
            12, 1))

    WebUI.setText(findTestObject('Object Repository/Login/input_Alias_inp-alias'), findTestData('Identitas').getValue(13, 
            1))

    WebUI.setText(findTestObject('Object Repository/Login/input__inp-tempat-lahir'), findTestData('Identitas').getValue(
            14, 1))

    WebUI.setText(findTestObject('Object Repository/Login/input__inp-tanggal-lahir'), findTestData('Identitas').getValue(
            15, 1))

    WebUI.selectOptionByValue(findTestObject('Object Repository/Login/select_--SILAHKAN PILIH--L - LAKI LAKIP - P_9c4a8a'), 
        findTestData('Identitas').getValue(16, 1), true)

    WebUI.setText(findTestObject('Object Repository/Login/input_Nama Gadis Ibu Kandung_inp-nama-ibu'), findTestData('Identitas').getValue(
            17, 1))

    WebUI.setText(findTestObject('Object Repository/Login/input_No Handphone-1_inp-no-hp-1'), findTestData('Identitas').getValue(
            18, 1))

    WebUI.setText(findTestObject('Object Repository/Login/input_No Handphone-2_inp-no-hp-2'), findTestData('Identitas').getValue(
            19, 1))

    WebUI.setText(findTestObject('Object Repository/Login/input_No Handphone-3_inp-no-hp-3'), findTestData('Identitas').getValue(
            20, 1))

    WebUI.click(findTestObject('Object Repository/Login/input_Kode Golongan Penjamin _btn-save-iden_a42964'))

    WebUI.delay(2)

    WebUI.click(findTestObject('Object Repository/Login/button_Ya'))

    String a = WebUI.getText(findTestObject('Import/get_no_aplikasi'))

    String excelFile = 'Data Files/simpan.xlsx'

    workbook = ExcelKeywords.getWorkbook(excelFile)

    sheet0 = ExcelKeywords.getExcelSheet(workbook, 'sheet0')

    ExcelKeywords.setValueToCellByIndex(sheet0, 1, 0, a)

    ExcelKeywords.saveWorkbook(excelFile, workbook)

    WebUI.click(findTestObject('Object Repository/Login/button_OK'))

    WebUI.delay(2)

    WebUI.delay(slow)
} else {
    WebUI.selectOptionByValue(findTestObject('Object Repository/Company/select_--SILAHKAN PILIH--001 - Private002 -_140e40'), 
        findTestData('Company - Input').getValue(1, 1), true)

    WebUI.setText(findTestObject('Company/input_Nama Lengkap Sesuai NPWP_inp-nama-npw_b00cee'), findTestData('Company - Input').getValue(
            2, 1))

    WebUI.setText(findTestObject('Object Repository/Company/input_Nama Perusahaan_inp-nama-perusahaan'), findTestData('Company - Input').getValue(
            3, 1))

    WebUI.click(findTestObject('Object Repository/Company/span_Sektor Ekonomi_fa fa-search'))

    WebUI.delay(slow)

    WebUI.setText(findTestObject('Object Repository/Company/input_Search_form-control input-sm'), findTestData('Company - Input').getValue(
            4, 1))

    WebUI.click(findTestObject('Pekerjaan_Pendapatan/TabelSektorEkonomiPekerjaan'))

    WebUI.click(findTestObject('Object Repository/Company/button_Pilih'))

    WebUI.click(findTestObject('Object Repository/Company/span_Lapangan Usaha_fa fa-search'))

    WebUI.delay(slow)

    WebUI.setText(findTestObject('Pekerjaan_Pendapatan/input_Search_Tabel_Lapangan_Pekerjaan'), findTestData('Company - Input').getValue(
            5, 1))

    WebUI.click(findTestObject('Pekerjaan_Pendapatan/TabelLapanganPekerjaan'))

    WebUI.click(findTestObject('Object Repository/Company/button_Pilih_1'))

    WebUI.selectOptionByValue(findTestObject('Object Repository/Company/select_--SILAHKAN PILIH--01 - MILIK SENDIRI_38f5b4'), 
        findTestData('Company - Input').getValue(6, 1), true)

    WebUI.selectOptionByValue(findTestObject('Object Repository/Company/select_--SILAHKAN PILIH--01 - RUMAH02 - PER_9c2b38'), 
        findTestData('Company - Input').getValue(7, 1), true)

    WebUI.setText(findTestObject('Object Repository/Company/input_Total Pegawai_inp-total-pegawai'), findTestData('Company - Input').getValue(
            8, 1))

    WebUI.setText(findTestObject('Object Repository/Company/input_Total Lama UsahaKerja(Th)_inp-lama-usaha'), findTestData(
            'Company - Input').getValue(9, 1))

    WebUI.click(findTestObject('Object Repository/Company/input_Tanggal Akta Pendirian Akhir_btn-save_0d63ab'))

    WebUI.delay(slow)

    WebUI.click(findTestObject('Object Repository/Company/button_Ya'))

    WebUI.click(findTestObject('Object Repository/Company/button_OK'))

    WebUI.delay(slow)

    WebUI.click(findTestObject('Object Repository/Company/input_Informasi Alamat_btn-tambah-info-alam_9676f7'))

    WebUI.delay(slow)

    WebUI.setText(findTestObject('Object Repository/Company/textarea_Alamat_inp-alamat-perusahaan'), findTestData('Alamat').getValue(
            1, 3))

    WebUI.setText(findTestObject('Object Repository/Company/input_RTRW_inp-rt-alamat-perusahaan'), findTestData('Alamat').getValue(
            3, 3))

    WebUI.setText(findTestObject('Object Repository/Company/input__inp-rw-alamat-perusahaan'), findTestData('Alamat').getValue(
            4, 3))

    WebUI.click(findTestObject('Object Repository/Company/span_Kode POS_fa fa-search'))

    WebUI.delay(slow)

    WebUI.setText(findTestObject('Object Repository/Company/input_Kode POS_inp-cari-kode-pos-perusahaan'), findTestData(
            'Alamat').getValue(5, 3))

    WebUI.click(findTestObject('Object Repository/Company/button_Cari'))

    WebUI.delay(slow)

    WebUI.click(findTestObject('Object Repository/Company/td_12910'))

    WebUI.click(findTestObject('Object Repository/Company/button_Pilih_1_2'))

    WebUI.setText(findTestObject('Object Repository/Company/input_Nomor Telp_inp-telp-alamat-perusahaan'), findTestData(
            'Alamat').getValue(6, 3))

    WebUI.setText(findTestObject('Object Repository/Company/input_Nomor Fax_inp-fax-alamat-perusahaan'), findTestData('Alamat').getValue(
            7, 3))

    WebUI.click(findTestObject('Object Repository/Company/input_Alamat Tagih_inp-alamat-tagih-perusahaan'))

    WebUI.click(findTestObject('Object Repository/Company/input_Alamat Tagih_btn-save-alamat-perusahaan'))

    WebUI.delay(slow)

    WebUI.click(findTestObject('Object Repository/Company/button_Ya'))

    WebUI.click(findTestObject('Object Repository/Company/button_OK'))

    WebUI.scrollToPosition(0, 0)

    WebUI.click(findTestObject('Object Repository/Company/a_Pendapatan'))

    WebUI.setText(findTestObject('Object Repository/Company/input_Pendapatan Perbulan_inp-pendapatan-pe_ecd0e4'), findTestData(
            'Company - Input').getValue(10, 1))

    WebUI.setText(findTestObject('Object Repository/Company/input_Pendapatan Lainnya_inp-pendapatan-lai_d62227'), findTestData(
            'Company - Input').getValue(11, 1))

    WebUI.setText(findTestObject('Object Repository/Company/input_Cust COGS_inp-cogs-perusahaan'), findTestData('Company - Input').getValue(
            12, 1))

    WebUI.setText(findTestObject('Object Repository/Company/input_Biaya Operasional_inp-biaya-operasion_1ad669'), findTestData(
            'Company - Input').getValue(13, 1))

    WebUI.setText(findTestObject('Object Repository/Company/input_Biaya Lainnya_inp-biaya-lainnya-perusahaan'), findTestData(
            'Company - Input').getValue(14, 1))

    WebUI.setText(findTestObject('Object Repository/Company/input_Pajak_inp-pajak-perusahaan'), findTestData('Company - Input').getValue(
            15, 1))

    WebUI.setText(findTestObject('Object Repository/Company/input_Angsuran Lainnya_inp-angsuran-lainnya_02b012'), findTestData(
            'Company - Input').getValue(16, 1))

    WebUI.click(findTestObject('Object Repository/Company/input_Perbandingan Pendapatan (DIR) _btn-sa_da6718'))

    WebUI.click(findTestObject('Object Repository/Company/button_Ya'))

    WebUI.click(findTestObject('Object Repository/Company/button_OK'))

    WebUI.scrollToPosition(0, 0)

    WebUI.click(findTestObject('Object Repository/Company/a_PIC Management'))

    WebUI.setText(findTestObject('Object Repository/Company/input_Nama Lengkap_inp-nama-lengkap-pic'), findTestData('Company - Input').getValue(
            17, 1))

    WebUI.click(findTestObject('Object Repository/Company/form_Jenis Identitas--SILAHKAN PILIH--01 - _d5a875'))

    WebUI.selectOptionByValue(findTestObject('Object Repository/Company/select_--SILAHKAN PILIH--YA                _2710ea'), 
        findTestData('Company - Input').getValue(18, 1), true)

    WebUI.selectOptionByValue(findTestObject('Object Repository/Company/select_--SILAHKAN PILIH--01-PEMILIK - Direk_78323c'), 
        findTestData('Company - Input').getValue(19, 1), true)

    WebUI.setText(findTestObject('Object Repository/Company/input_No Handphone_inp-hp-pic'), findTestData('Company - Input').getValue(
            20, 1))

    WebUI.click(findTestObject('Object Repository/Company/input_Status PengurusPemilik _btn-save-pic-_a9a35b'))

    WebUI.delay(slow)

    WebUI.click(findTestObject('Object Repository/Company/button_Ya'))

    WebUI.click(findTestObject('Object Repository/Company/button_OK'))
}

