import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

int slow = 2

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/Aplikasi/a_Aplikasi'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Aplikasi/select_--SILAHKAN PILIH--02030000000 - KARAWANG'), 
    findTestData('Aplikasi').getValue(1, 1), true)

WebUI.setText(findTestObject('Object Repository/Aplikasi/input_Tanggal Pemesanan_inp-aplikasi-order-date'), findTestData(
        'Aplikasi').getValue(3, 1))

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi/select_--SILAHKAN PILIH--2 - SYARIAH1 - KON_4bb280'), 
    findTestData('Aplikasi').getValue(4, 1), true)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi/select_--SILAHKAN PILIH--01 - INVESTASI02 -_8376cb'), 
    findTestData('Aplikasi').getValue(5, 1), true)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi/select_--SILAHKAN PILIH--01 - DEALERSHOWROO_7d5858'), 
    findTestData('Aplikasi').getValue(6, 1), true)

if (findTestData('Aplikasi').getValue(6, 1) == '01 - DEALER/SHOWROOM') {
    WebUI.click(findTestObject('Object Repository/Aplikasi/span_Outlet Channel_fa fa-search'))

    WebUI.setText(findTestObject('Aplikasi/Search_Outlet'), findTestData('Aplikasi').getValue(7, 1))

    WebUI.click(findTestObject('Aplikasi/TabelListOutlet'))

    WebUI.click(findTestObject('Object Repository/Aplikasi/button_Pilih'))
} else {
    if (findTestData('Aplikasi').getValue(6, 1) == '02 - BMRI') {
        WebUI.click(findTestObject('Object Repository/Aplikasi/Aplikasi2/span_Outlet Channel_fa fa-search'))

        WebUI.setText(findTestObject('Object Repository/Aplikasi/Aplikasi2/input_Location_inp-cari-location-bmri'), findTestData(
                'Aplikasi').getValue(7, 1))

        WebUI.click(findTestObject('Object Repository/Aplikasi/Aplikasi2/button_Cari'))

        WebUI.delay(slow)

        WebUI.setText(findTestObject('Aplikasi/Aplikasi2/SrcOutletBMRI'), findTestData('Aplikasi').getValue(23, 1))

        WebUI.click(findTestObject('Aplikasi/Aplikasi2/TabelOutletBMRI'))

        WebUI.click(findTestObject('Object Repository/Aplikasi/Aplikasi2/button_Pilih'))

        WebUI.click(findTestObject('Object Repository/Aplikasi/Aplikasi2/span_Group Customer_fa fa-search'))

        WebUI.delay(slow)

        WebUI.setText(findTestObject('Aplikasi/Aplikasi2/SrcGroupCustomer'), findTestData('Aplikasi').getValue(24, 1))

        WebUI.click(findTestObject('Aplikasi/Aplikasi2/Tabel_Group_Customer'))

        WebUI.click(findTestObject('Object Repository/Aplikasi/Aplikasi2/button_Pilih_1'))
    }
}

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi/select_--SILAHKAN PILIH--01-KENDARAAN08-STA_ea6212'), 
    findTestData('Aplikasi').getValue(8, 1), true)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi/select_--SILAHKAN PILIH--11-NASABAH BMRI LA_f5f5e3'), 
    findTestData('Aplikasi').getValue(9, 1), true)

WebUI.setText(findTestObject('Object Repository/Aplikasi/input_Waktu Survey_inp-aplikasi-janji-survey'), findTestData('Aplikasi').getValue(
        10, 1))

WebUI.click(findTestObject('Object Repository/Aplikasi/input_Tidak_optradio'))

if (findTestData('Aplikasi').getValue(8, 1) != '01-KENDARAAN') {
    WebUI.selectOptionByValue(findTestObject('Aplikasi/Aplikasi2/select_--SILAHKAN PILIH--                  _71ba45'), findTestData(
            'Aplikasi').getValue(19, 1), true)

    if (findTestData('Aplikasi').getValue(19, 1) == '1') {
        WebUI.click(findTestObject('Object Repository/Aplikasi/Aplikasi2/span_Bank_fa fa-search'))

        WebUI.delay(slow)

        WebUI.setText(findTestObject('Aplikasi/Aplikasi2/SrcDaftarBank'), findTestData('Aplikasi').getValue(20, 1))

        WebUI.click(findTestObject('Aplikasi/Aplikasi2/TabelListBankTransferRek'))

        WebUI.click(findTestObject('Object Repository/Aplikasi/Aplikasi2/button_Pilih_1_2'))

        WebUI.setText(findTestObject('Object Repository/Aplikasi/Aplikasi2/input_Account No_inp-cust-rek-acc-no'), findTestData(
                'Aplikasi').getValue(21, 1))

        WebUI.setText(findTestObject('Object Repository/Aplikasi/Aplikasi2/input_Account Name_inp-cust-rek-acc-name'), findTestData(
                'Aplikasi').getValue(22, 1))
    }
} else {
    if (findTestData('Aplikasi').getValue(11, 1) == '01 - KASIR') {
        WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi/select_--SILAHKAN PILIH--01 - KASIR02 - KOL_1ecc07'), 
            findTestData('Aplikasi').getValue(11, 1), true)
    } else if (findTestData('Aplikasi').getValue(11, 1) == '04 - AUTO DEBET') {
        WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi/select_--SILAHKAN PILIH--01 - KASIR02 - KOL_1ecc07'), 
            findTestData('Aplikasi').getValue(11, 1), true)

        WebUI.setText(findTestObject('Object Repository/Aplikasi/Aplikasi2/input_Account No_inp-aplikasi-acc-no'), findTestData(
                'Aplikasi').getValue(12, 1))

        WebUI.setText(findTestObject('Object Repository/Aplikasi/Aplikasi2/input_Account Name_inp-aplikasi-acc-name'), findTestData(
                'Aplikasi').getValue(13, 1))
    }
}

WebUI.click(findTestObject('Object Repository/Aplikasi/button_Save'))

WebUI.delay(slow)

WebUI.click(findTestObject('Object Repository/Aplikasi/button_Ya'))

WebUI.click(findTestObject('Object Repository/Aplikasi/button_OK'))

WebUI.delay(slow)

WebUI.click(findTestObject('Object Repository/Aplikasi/button_Tambah'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Aplikasi/select_--SILAHKAN PILIH--                  _7bd9d2'), 
    findTestData('Aplikasi').getValue(14, 1), true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Aplikasi/select_--SILAHKAN PILIH--BRO-BUSINESS RELAT_f19ffc'), 
    findTestData('Aplikasi').getValue(15, 1), true)

WebUI.click(findTestObject('Object Repository/Aplikasi/span_Internal Sales Force_fa fa-search'))

WebUI.delay(slow)

WebUI.setText(findTestObject('Aplikasi/Search_internal_sales_employee'), findTestData('Aplikasi').getValue(16, 1))

WebUI.click(findTestObject('Aplikasi/TabelInternalSalesEmployee'))

WebUI.click(findTestObject('Object Repository/Aplikasi/button_Pilih_1'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Aplikasi/select_--SILAHKAN PILIH--                  _c00451'), 
    findTestData('Aplikasi').getValue(17, 1), true)

WebUI.click(findTestObject('Object Repository/Aplikasi/span_Internal Sales Head_fa fa-search'))

WebUI.delay(slow)

WebUI.setText(findTestObject('Aplikasi/Search_internal_sales_head'), findTestData('Aplikasi').getValue(18, 1))

WebUI.click(findTestObject('Aplikasi/TabelInternalSalesHead'))

WebUI.click(findTestObject('Object Repository/Aplikasi/button_Pilih_1_2'))

WebUI.click(findTestObject('Object Repository/Aplikasi/input_Internal Sales Head_btn-ide-aplikasi-_2b95f0'))

WebUI.delay(slow)

WebUI.click(findTestObject('Object Repository/Aplikasi/button_Ya'))

WebUI.click(findTestObject('Object Repository/Aplikasi/button_OK'))

if ((findTestData('Aplikasi').getValue(6, 1) == '02 - BMRI') || (findTestData('Aplikasi').getValue(6, 1) == '03 - MITRA MANDIRI')) {
    WebUI.click(findTestObject('Object Repository/Aplikasi/Aplikasi2/button_Tambah'))

    WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi/Aplikasi2/select_--SILAHKAN PILIH--                  _41556d'), 
        findTestData('Aplikasi').getValue(25, 1), true)

    WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi/Aplikasi2/select_--SILAHKAN PILIH--FRL-FRONTLINER    _8c7cc5'), 
        findTestData('Aplikasi').getValue(26, 1), true)

    WebUI.click(findTestObject('Object Repository/Aplikasi/Aplikasi2/button_External Sales Force_btn-ide-aplikas_1ab3a0'))

    if (findTestData('Aplikasi').getValue(6, 1) == '02 - BMRI') {
        WebUI.setText(findTestObject('Aplikasi/Aplikasi2/input_nama_emp_bmri'), findTestData('Aplikasi').getValue(27, 1))

        WebUI.click(findTestObject('Aplikasi/Aplikasi2/btn-cari-emp-bmri'))

        WebUI.click(findTestObject('Aplikasi/Aplikasi2/TabelExtBMRI'))

        WebUI.click(findTestObject('Aplikasi/Aplikasi2/button_Pilih_ext_bmri'))
    } else {
        WebUI.setText(findTestObject('Aplikasi/Aplikasi2/input_nama_mitra'), findTestData('Aplikasi').getValue(27, 1))

        WebUI.click(findTestObject('Aplikasi/Aplikasi2/btn-cari-emp-mitra'))

        WebUI.click(findTestObject('Aplikasi/Aplikasi2/TabelExtMitra'))

        WebUI.click(findTestObject('Aplikasi/Aplikasi2/button_Pilih_ext_mitra'))
    }
    
    WebUI.click(findTestObject('Object Repository/Aplikasi/Aplikasi2/input_Komisi Perantara_btn-ide-aplikasi-ext_8e4814'))

    WebUI.click(findTestObject('Object Repository/Aplikasi/Aplikasi2/button_Ya'))

    WebUI.click(findTestObject('Object Repository/Aplikasi/Aplikasi2/button_OK'))
}

