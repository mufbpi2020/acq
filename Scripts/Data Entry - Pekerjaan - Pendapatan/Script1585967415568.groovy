import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

int slow = 0

WebUI.scrollToPosition(0, 0)

if (findTestData('Pekerjaan').getValue(1, 1) == 'Pegawai') {
    WebUI.click(findTestObject('Pekerjaan_Pendapatan/a_Pekerjaan'))

    WebUI.delay(2)

    WebUI.selectOptionByLabel(findTestObject('Object Repository/Pekerjaan_Pendapatan/select_--SILAHKAN PILIH--001 - AccountingFi_20d0c1'), 
        findTestData('Pekerjaan').getValue(2, 1), true)

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Nama Perusahaan_inp-nama-perusahaan-karyawan'), 
        findTestData('Pekerjaan').getValue(3, 1))

    WebUI.selectOptionByLabel(findTestObject('Object Repository/Pekerjaan_Pendapatan/select_--SILAHKAN PILIH--001 - Private002 -_140e40'), 
        findTestData('Pekerjaan').getValue(4, 1), true)

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/span_Sektor Ekonomi_fa fa-search corr-disab_7835bd'))

    WebUI.delay(slow)

    WebUI.setText(findTestObject('Pekerjaan_Pendapatan/input_Search_Tabel_Sektor_Ekonomi'), findTestData('Pekerjaan').getValue(
            5, 1))

    WebUI.click(findTestObject('Pekerjaan_Pendapatan/TabelSektorEkonomiPekerjaan'))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_Pilih'))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/span_Lapangan Usaha_fa fa-search corr-disab_1eda6d'))

    WebUI.delay(slow)

    WebUI.setText(findTestObject('Pekerjaan_Pendapatan/input_Search_Tabel_Lapangan_Pekerjaan'), findTestData('Pekerjaan').getValue(
            6, 1))

    WebUI.click(findTestObject('Pekerjaan_Pendapatan/TabelLapanganPekerjaan'))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_Pilih_1'))

    WebUI.selectOptionByValue(findTestObject('Object Repository/Pekerjaan_Pendapatan/select_--SILAHKAN PILIH--0004 - TIDAK ADA00_8adeb2'), 
        findTestData('Pekerjaan').getValue(7, 1), true)

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Total Pegawai_inp-total-pegawai-karyawan'), 
        findTestData('Pekerjaan').getValue(8, 1))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Total Lama UsahaKerja(Th)_inp-total-l_5a36f0'), 
        findTestData('Pekerjaan').getValue(9, 1))

    WebUI.selectOptionByValue(findTestObject('Object Repository/Pekerjaan_Pendapatan/select_--SILAHKAN PILIH--01 - TETAP02 - KON_eae7f6'), 
        findTestData('Pekerjaan').getValue(10, 1), true)

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/textarea_Alamat_inp-alamat-karyawan'), findTestData(
            'Pekerjaan').getValue(11, 1))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_RTRW_inp-rt-alamat-karyawan'), findTestData(
            'Pekerjaan').getValue(12, 1))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input__inp-rw-alamat-karyawan'), findTestData('Pekerjaan').getValue(
            13, 1))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/span_Kode POS_fa fa-search corr-disabled'))

    WebUI.delay(slow)

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Kode POS_inp-cari-kode-pos'), findTestData(
            'Pekerjaan').getValue(14, 1))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_Cari'))

    WebUI.delay(slow)

    WebUI.click(findTestObject('Pekerjaan_Pendapatan/TabelKodePosPekerjaan'))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_Pilih_1_2'))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Provinsi_btn-save-karyawan'))

    WebUI.delay(slow)

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_Ya'))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_OK'))

    WebUI.delay(slow)

    WebUI.scrollToPosition(0, 0)

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/a_Pendapatan'))

    WebUI.delay(slow)

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Pendapatan Perbulan_inp-pendapatan-pe_9d871c'), 
        findTestData('Pekerjaan').getValue(17, 1))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Pendapatan Lainnya_inp-pendapatan-lai_4403eb'), 
        findTestData('Pekerjaan').getValue(18, 1))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Biaya Hidup_inp-biaya-hidup-prokar'), findTestData(
            'Pekerjaan').getValue(19, 1))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Angsuran Lainnya_inp-angsuran-lainnya-prokar'), 
        findTestData('Pekerjaan').getValue(20, 1))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Perbandingan Pendapatan (DIR) _btn-sa_6415b4'))

    WebUI.delay(slow)

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_Ya'))

    WebUI.delay(slow)

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_OK'))
} else {
    WebUI.click(findTestObject('Pekerjaan_Pendapatan/a_Pekerjaan'))

    WebUI.delay(2)

    WebUI.selectOptionByLabel(findTestObject('Object Repository/Pekerjaan_Pendapatan/select_--SILAHKAN PILIH--001 - AccountingFi_20d0c1'), 
        findTestData('Pekerjaan').getValue(2, 2), true)

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Nama Usaha_inp-nama-usaha-wiraswasta'), findTestData(
            'Pekerjaan').getValue(3, 2))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/span_Sektor Ekonomi_fa fa-search corr-disabled'))

    WebUI.setText(findTestObject('Pekerjaan_Pendapatan/input_Search_Tabel_Sektor_Ekonomi'), findTestData('Pekerjaan').getValue(
            5, 2))

    WebUI.click(findTestObject('Pekerjaan_Pendapatan/TabelSektorEkonomiPekerjaan'))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_Pilih'))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/span_Lapangan Usaha_fa fa-search corr-disabled'))

    WebUI.setText(findTestObject('Pekerjaan_Pendapatan/input_Search_Tabel_Lapangan_Pekerjaan'), findTestData('Pekerjaan').getValue(
            6, 2))

    WebUI.click(findTestObject('Pekerjaan_Pendapatan/TabelLapanganPekerjaan'))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_Pilih_1'))

    WebUI.selectOptionByValue(findTestObject('Object Repository/Pekerjaan_Pendapatan/select_--SILAHKAN PILIH--01 - MILIK SENDIRI_38f5b4'), 
        findTestData('Pekerjaan').getValue(15, 2), true)

    WebUI.selectOptionByValue(findTestObject('Object Repository/Pekerjaan_Pendapatan/select_--SILAHKAN PILIH--01 - RUMAH02 - PER_9c2b38'), 
        findTestData('Pekerjaan').getValue(16, 2), true)

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Total Pegawai_inp-total-pegawai-wiraswasta'), 
        findTestData('Pekerjaan').getValue(8, 2))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Total Lama UsahaKerja(Th)_inp-total-l_33f9d9'), 
        findTestData('Pekerjaan').getValue(9, 2))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/textarea_Alamat_inp-alamat-wiraswasta'), findTestData(
            'Pekerjaan').getValue(11, 2))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_RTRW_inp-rt-alamat-wiraswasta'), findTestData(
            'Pekerjaan').getValue(12, 2))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input__inp-rw-alamat-wiraswasta'), findTestData(
            'Pekerjaan').getValue(13, 2))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/span_Kode POS_fa fa-search corr-disabled_1'))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Kode POS_inp-cari-kode-pos'), findTestData(
            'Pekerjaan').getValue(14, 2))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_Cari'))

    WebUI.delay(slow)

    WebUI.click(findTestObject('Pekerjaan_Pendapatan/TabelKodePosPekerjaan'))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_Pilih_1_2'))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Provinsi_btn-save-wiraswasta'))

    WebUI.delay(slow)

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_Ya'))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_OK'))

    WebUI.delay(slow)

    WebUI.scrollToPosition(0, 0)

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/a_Pendapatan'))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Laba Kotor_inp-laba-kotor-wiraswasta'), findTestData(
            'Pekerjaan').getValue(17, 2))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Biaya Operasional_inp-biaya-operasion_f3d060'), 
        findTestData('Pekerjaan').getValue(18, 2))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Biaya Hidup_inp-biaya-hidup-wiraswasta'), 
        findTestData('Pekerjaan').getValue(19, 2))

    WebUI.setText(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Angsuran Lainnya_inp-angsuran-lainnya_de88a8'), 
        findTestData('Pekerjaan').getValue(20, 2))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/input_Perbandingan Pendapatan (DIR) _btn-sa_78db9c'))

    WebUI.delay(slow)

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_Ya'))

    WebUI.click(findTestObject('Object Repository/Pekerjaan_Pendapatan/button_OK'))
}

