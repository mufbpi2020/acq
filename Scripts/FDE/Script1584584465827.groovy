import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(findTestData('Login').getValue(1, 1))

WebUI.setText(findTestObject('Login/input_Silahkan Login_id-login'), findTestData('Login').getValue(2, 1))

WebUI.setText(findTestObject('Login/input_Silahkan Login_pass-login'), findTestData('Login').getValue(3, 1))

WebUI.click(findTestObject('Login/button_Login'))

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/FDE/a_DATA ENTRY COMPLETION'))

WebUI.click(findTestObject('Object Repository/FDE/button_Search To Do List'))

WebUI.setText(findTestObject('FDE/src_no_aplikasi'), findTestData('Import').getValue(2, 1))

WebUI.click(findTestObject('Object Repository/FDE/i_FDEDRC_fa fa-pencil'))

WebUI.selectOptionByLabel(findTestObject('Object Repository/FDE/select_--SILAHKAN PILIH--01 - ISLAM02 - KRI_919132'), findTestData(
        'FDE').getValue(2, 1), true)

WebUI.selectOptionByLabel(findTestObject('Object Repository/FDE/select_--SILAHKAN PILIH--WNI - WARGA NEGARA_9ac1e0'), findTestData(
        'FDE').getValue(3, 1), true)

WebUI.setText(findTestObject('Object Repository/FDE/input_Nomor Kartu Keluarga_inp-no-kartu-keluarga'), findTestData('FDE').getValue(
        4, 1))

WebUI.selectOptionByLabel(findTestObject('Object Repository/FDE/select_--SILAHKAN PILIH--31 - BE29 - SKM01 _e0a0b8'), findTestData(
        'FDE').getValue(5, 1), true)

WebUI.selectOptionByLabel(findTestObject('Object Repository/FDE/select_--SILAHKAN PILIH--                  _3f4cf9'), findTestData(
        'FDE').getValue(6, 1), true)

if (findTestData('FDE').getValue(6, 1) == '1 - YA') {
    WebUI.setText(findTestObject('Object Repository/FDE/input_Nomor NPWP _inp-no-npwp'), '111111118111111')

    WebUI.setText(findTestObject('Object Repository/FDE/input_Nama Lengkap Sesuai NPWP_inp-nama-npwp'), findTestData('FDE').getValue(
            8, 1))
}

WebUI.click(findTestObject('Object Repository/FDE/input_Kode Golongan Penjamin _btn-save-iden_a42964'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/FDE/button_Ya'))

WebUI.click(findTestObject('Object Repository/FDE/button_OK'))

WebUI.click(findTestObject('Object Repository/FDE/button_Tambah'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/FDE/select_--SILAHKAN PILIH--01-IBU02-AYAH03-AN_0ea8ce'))

WebUI.selectOptionByLabel(findTestObject('Object Repository/FDE/select_--SILAHKAN PILIH--01-IBU02-AYAH03-AN_0ea8ce'), findTestData(
        'FDE').getValue(9, 1), true)

WebUI.selectOptionByLabel(findTestObject('Object Repository/FDE/select_--SILAHKAN PILIH--01 - KTP03 - PASSP_acec17'), findTestData(
        'FDE').getValue(10, 1), true)

WebUI.setText(findTestObject('Object Repository/FDE/input_Nomor Identitas_inp-no-identitas-keluarga'), findTestData('FDE').getValue(
        11, 1))

WebUI.setText(findTestObject('Object Repository/FDE/input_Tanggal Berlaku Identitas_inp-issued-_10fb21'), findTestData('FDE').getValue(
        12, 1))

WebUI.setText(findTestObject('Object Repository/FDE/input_sd_inp-expired-date-keluarga'), findTestData('FDE').getValue(13, 
        1))

WebUI.setText(findTestObject('Object Repository/FDE/input_Nama Lengkap_inp-nama-lengkap-keluarga'), findTestData('FDE').getValue(
        14, 1))

WebUI.selectOptionByLabel(findTestObject('Object Repository/FDE/select_--SILAHKAN PILIH--L - LAKI LAKIP - P_6aacfb'), findTestData(
        'FDE').getValue(15, 1), true)

WebUI.setText(findTestObject('Object Repository/FDE/textarea_Alamat_inp-alamat-keluarga'), findTestData('FDE').getValue(
        16, 1))

WebUI.setText(findTestObject('Object Repository/FDE/input_RTRW_inp-rt-keluarga'), findTestData('FDE').getValue(17, 1))

WebUI.setText(findTestObject('Object Repository/FDE/input__inp-rw-keluarga'), findTestData('FDE').getValue(18, 1))

WebUI.click(findTestObject('Object Repository/FDE/span_Kode POS_fa fa-search'))

WebUI.setText(findTestObject('Object Repository/FDE/input_Kode POS_inp-cari-kode-pos'), findTestData('FDE').getValue(19, 
        1))

WebUI.click(findTestObject('Object Repository/FDE/button_Cari'))

WebUI.click(findTestObject('Object Repository/FDE/td_14310'))

WebUI.click(findTestObject('Object Repository/FDE/button_Pilih'))

WebUI.setText(findTestObject('Object Repository/FDE/input_Nomor Telepon Tetap_inp-no-telp-keluarga'), findTestData('FDE').getValue(
        20, 1))

WebUI.click(findTestObject('FDE/input_Emergency Contact_inp-emergency-contact-keluarga'))

WebUI.click(findTestObject('Object Repository/FDE/input_Emergency Contact_btn-save-info-keluarga'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/FDE/button_Ya'))

WebUI.click(findTestObject('Object Repository/FDE/button_OK'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/FDE/a_Aplikasi'))

WebUI.selectOptionByLabel(findTestObject('Object Repository/FDE/select_--SILAHKAN PILIH--10 - Kredit Modal _660fd4'), findTestData(
        'FDE').getValue(21, 1), true)

WebUI.click(findTestObject('Object Repository/FDE/span_Golongan Kredit_fa fa-search'))

WebUI.setText(findTestObject('FDE/src_gol_kredit'), findTestData('FDE').getValue(22, 1))

WebUI.click(findTestObject('Object Repository/FDE/td_10'))

WebUI.click(findTestObject('Object Repository/FDE/button_Pilih_1'))

WebUI.selectOptionByLabel(findTestObject('Object Repository/FDE/select_--SILAHKAN PILIH--10 - Dalam rangka _ef839f'), findTestData(
        'FDE').getValue(23, 1), true)

WebUI.selectOptionByLabel(findTestObject('Object Repository/FDE/select_--SILAHKAN PILIH--01 - GAJI02 - BONU_13b873'), findTestData(
        'FDE').getValue(24, 1), true)

WebUI.click(findTestObject('Object Repository/FDE/button_Save'))

WebUI.click(findTestObject('Object Repository/FDE/button_Ya'))

WebUI.click(findTestObject('Object Repository/FDE/button_OK'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/FDE/a_Objek Pembiayaan'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/FDE/i_HYUNDAI_fa fa-pencil'))

WebUI.setText(findTestObject('Object Repository/FDE/input_Tahun Pembuatan _inp-obj-year'), findTestData('FDE').getValue(
        25, 1))

WebUI.setText(findTestObject('Object Repository/FDE/input_Tanggal Expire STNK _inp-exp-stnk'), findTestData('FDE').getValue(
        26, 1))

if (findTestData('FDE').getValue(27, 1) == 'sama') {
    WebUI.click(findTestObject('Object Repository/FDE/input_BPKB sama dengan nama nasabah_bpkb-flag'))
} else {
    WebUI.click(findTestObject('Object Repository/FDE/input_BPKB beda dengan nama nasabah_bpkb-flag'))
}

WebUI.selectOptionByLabel(findTestObject('Object Repository/FDE/select_--SILAHKAN PILIH--01 - IBU02 - AYAH0_c98b5d'), findTestData(
        'FDE').getValue(32, 1), true)

WebUI.setText(findTestObject('Object Repository/FDE/textarea_Alamat BPKB _inp-almt-bpkb'), findTestData('FDE').getValue(
        30, 1))

WebUI.click(findTestObject('Object Repository/FDE/input_No Invoice _btn-save-objek-pembiayaan_2d5fc7'))

WebUI.click(findTestObject('Object Repository/FDE/button_Ya'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/FDE/button_OK'))

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/FDE/a_Struktur Kredit'))

WebUI.delay(2)

WebUI.selectOptionByLabel(findTestObject('Object Repository/FDE/select_--SILAHKAN PILIH--280 - IRREVOCABLE _eb3a0d'), findTestData(
        'FDE').getValue(31, 1), true)

WebUI.click(findTestObject('Object Repository/FDE/input_Angsuran Terakhir_btn-save-rincian-pinjaman'))

WebUI.click(findTestObject('Object Repository/FDE/button_Ya'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/FDE/button_OK'))

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/FDE/button_Konfirmasi'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/FDE/button_Ya'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/FDE/button_OK'))

WebUI.closeBrowser()

