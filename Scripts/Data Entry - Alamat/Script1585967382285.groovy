import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

int slow = 0

for (def index : (1..2)) {
    WebUI.click(findTestObject('Object Repository/Login/button_Tambah'))

    WebUI.delay(slow)

    WebUI.setText(findTestObject('Object Repository/Login/textarea_Alamat_inp-alamat-perorangan'), findTestData('Alamat').getValue(
            1, index))

    WebUI.selectOptionByValue(findTestObject('Object Repository/Login/select_--SILAHKAN PILIH--01-ALAMAT KTP03-AL_4f6bcc'), 
        findTestData('Alamat').getValue(2, index), true)

    WebUI.setText(findTestObject('Object Repository/Login/input_RTRW_inp-rt-alamat-perorangan'), findTestData('Alamat').getValue(
            3, index))

    WebUI.setText(findTestObject('Object Repository/Login/input__inp-rw-alamat-perorangan'), findTestData('Alamat').getValue(
            4, index))

    WebUI.click(findTestObject('Object Repository/Login/span_Kode POS_fa fa-search'))

    WebUI.setText(findTestObject('Object Repository/Login/input_Kode POS_inp-cari-kode-pos'), findTestData('Alamat').getValue(
            5, index))

    WebUI.click(findTestObject('Object Repository/Login/button_Cari'))

    WebUI.click(findTestObject('Login/Tabel_Kodepos_Alamat'))

    WebUI.click(findTestObject('Object Repository/Login/button_Pilih'))

    WebUI.setText(findTestObject('Object Repository/Login/input_Nomor Telp_inp-telp-alamat-perorangan'), findTestData('Alamat').getValue(
            6, index))

    WebUI.setText(findTestObject('Object Repository/Login/input_Nomor Fax_inp-fax-alamat-perorangan'), findTestData('Alamat').getValue(
            7, index))

    WebUI.click(findTestObject('Object Repository/Login/input_Alamat Tagih_btn-save-info-alamat'))

    WebUI.click(findTestObject('Object Repository/Login/button_Ya'))

    WebUI.delay(slow)

    WebUI.click(findTestObject('Object Repository/Login/button_OK'))

    WebUI.delay(slow)

}
WebUI.delay(2)
