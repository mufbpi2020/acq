import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/a_Survey'))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Jumlah Kepemilikan Mobil_kepemilikan-mobil'), findTestData(
        'Survey').getValue(1, 1))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Jumlah Kepemilikan Motor_kepemilikan-motor'), findTestData(
        'Survey').getValue(2, 1))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Harga Rumah (Rp)_harga-rumah'), findTestData('Survey').getValue(
        3, 1))

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--01 - MILIK SENDIRI_54321b'), 
    findTestData('Survey').getValue(4, 1), true)

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Luas Bangunan M_luas-bangunan'), findTestData('Survey').getValue(
        5, 1))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Luas Tanah M_luas-tanah'), findTestData('Survey').getValue(
        6, 1))

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--00 - -01 - ASPAL02_a8fccd'), 
    findTestData('Survey').getValue(7, 1), true)

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--00 - 0 WATT01 - 45_c59e78'), 
    findTestData('Survey').getValue(8, 1), true)

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Tagihan Listrik Bulanan (Rp)_tagihan-listrik'), findTestData(
        'Survey').getValue(9, 1))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Lama Tinggal (Th)_lama-tinggal'), findTestData('Survey').getValue(
        10, 1))

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--0 - RUMAH1 - KANTO_68d98c'), 
    findTestData('Survey').getValue(11, 1), true)

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Jarak Rumah Customer dengan MUF (Km)__3cb64c'), findTestData(
        'Survey').getValue(12, 1))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Jarak Kantor  Usaha Customer dengan K_75cd29'), findTestData(
        'Survey').getValue(13, 1))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Jarak Rumah Customer dengan Dealer (K_f13d57'), findTestData(
        'Survey').getValue(14, 1))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Jarak Lokasi Penggunaan Unit  Pool de_30f3f6'), findTestData(
        'Survey').getValue(15, 1))

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/button_Save'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/button_Ya'))

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/button_OK'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/a_Hasil Survey'))

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Sumber Info Survey 1_sumber-info-survey1'), findTestData(
        'Survey').getValue(16, 1))

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--01 - REPUTASI BAIK_16cd3b'), 
    findTestData('Survey').getValue(17, 1), true)

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/input_Sumber Info Survey 2_sumber-info-survey2'), findTestData(
        'Survey').getValue(18, 1))

WebUI.selectOptionByLabel(findTestObject('Object Repository/Aplikasi-Survey/select_--SILAHKAN PILIH--01 - REPUTASI BAIK_16cd3b_1'), 
    findTestData('Survey').getValue(19, 1), true)

WebUI.setText(findTestObject('Object Repository/Aplikasi-Survey/textarea_Survey Notes_survey-notes'), findTestData('Survey').getValue(
        1, 1))

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/button_Save_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/button_Ya'))

WebUI.click(findTestObject('Object Repository/Aplikasi-Survey/button_OK'))

