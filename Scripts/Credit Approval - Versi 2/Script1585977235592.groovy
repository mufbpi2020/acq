import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.awt.List as List
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.Keys as Keys

int slow = 2

int tunggu = 3

WebUI.callTestCase(findTestCase('Go To Link'), [:], FailureHandling.STOP_ON_FAILURE)

//Approval Level 1 - 121 - CREDIT ANALYST
WebUI.setText(findTestObject('Login/input_Silahkan Login_id-login'), findTestData('Credit Approval').getValue(2, 2))

WebUI.setText(findTestObject('Login/input_Silahkan Login_pass-login'), findTestData('Login').getValue(3, 1))

WebUI.click(findTestObject('Login/button_Login'))

WebUI.click(findTestObject('credit_approval/a_CREDIT APPROVAL PROCESS'))

//jika option enable approval 1
if (WebUI.verifyOptionPresentByLabel(findTestObject('credit_approval/pilih_job_approval'), '--SILAHKAN PILIH--', true, tunggu, 
    FailureHandling.OPTIONAL)) {
    WebUI.selectOptionByLabel(findTestObject('Object Repository/credit_approval/select_--SILAHKAN PILIH--165 - OPERATION DI_dd42a4'), 
        findTestData('Credit Approval').getValue(2, 1), true)
}

if (WebUI.verifyOptionNotPresentByLabel(findTestObject('credit_approval/pilih_cabang'), findTestData('Credit Approval').getValue(
        10, 1), true, tunggu, FailureHandling.OPTIONAL)) {
    WebUI.selectOptionByLabel(findTestObject('credit_approval/pilih_cabang'), findTestData('Credit Approval').getValue(9, 
            1), false)
}

WebUI.click(findTestObject('credit_approval/button_Find'))

WebUI.setText(findTestObject('credit_approval/input_Search_form-control input-sm'), findTestData('Import').getValue(2, 1))

if (WebUI.verifyElementPresent(findTestObject('credit_approval/i_Tidak_fa fa-pencil'), tunggu, FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('credit_approval/i_Tidak_fa fa-pencil'))

    WebUI.click(findTestObject('credit_approval/tabel_dedup_ca'))

    WebUI.click(findTestObject('credit_approval/button_To View Detail'))

    WebUI.click(findTestObject('credit_approval/button_Ya'))

    WebUI.delay(slow)

    WebUI.callTestCase(findTestCase('Credit Approval - Function - Click Tab'), [:])

    WebUI.selectOptionByLabel(findTestObject('credit_approval/select_--SILAHKAN PILIH--APPD-Approve  Docu_f606d3'), findTestData(
            'Credit Approval').getValue(2, 3), true)

    WebUI.callTestCase(findTestCase('Credit Approval - Function - Approval v2'), [:])

    WebUI.callTestCase(findTestCase('ACQ - Logout'), [:])
} else {
    WebUI.callTestCase(findTestCase('ACQ - Logout'), [:])
}

//Approval 2 - 200 - BRANCH MANAGER
WebUI.setText(findTestObject('Login/input_Silahkan Login_id-login'), findTestData('Credit Approval').getValue(3, 2))

WebUI.setText(findTestObject('Login/input_Silahkan Login_pass-login'), '3')

WebUI.click(findTestObject('Login/button_Login'))

WebUI.click(findTestObject('credit_approval/a_CREDIT APPROVAL PROCESS'))

//jika option enable approval 2
if (WebUI.verifyOptionPresentByLabel(findTestObject('credit_approval/pilih_job_approval'), '--SILAHKAN PILIH--', true, tunggu, 
    FailureHandling.OPTIONAL)) {
    WebUI.selectOptionByLabel(findTestObject('Object Repository/credit_approval/select_--SILAHKAN PILIH--165 - OPERATION DI_dd42a4'), 
        findTestData('Credit Approval').getValue(3, 1), true)
}

if (WebUI.verifyOptionNotPresentByLabel(findTestObject('credit_approval/pilih_cabang'), findTestData('Credit Approval').getValue(
        10, 1), true, tunggu, FailureHandling.OPTIONAL)) {
    WebUI.selectOptionByLabel(findTestObject('credit_approval/pilih_cabang'), findTestData('Credit Approval').getValue(9, 
            1), false)
}

WebUI.click(findTestObject('credit_approval/button_Find'))

WebUI.setText(findTestObject('credit_approval/input_Search_form-control input-sm'), findTestData('Import').getValue(2, 1))

if (WebUI.verifyElementPresent(findTestObject('credit_approval/i_Tidak_fa fa-pencil'), tunggu, FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('credit_approval/i_Tidak_fa fa-pencil'))

    WebUI.callTestCase(findTestCase('Credit Approval - Function - Click Tab'), [:])

    WebUI.selectOptionByLabel(findTestObject('credit_approval/select_--SILAHKAN PILIH--APPD-Approve  Docu_f606d3'), findTestData(
            'Credit Approval').getValue(3, 3), true)

    WebUI.callTestCase(findTestCase('Credit Approval - Function - Approval v2'), [:])

    WebUI.callTestCase(findTestCase('ACQ - Logout'), [:])
} else {
    WebUI.callTestCase(findTestCase('ACQ - Logout'), [:])
}

//Approval 3 - 216 - AREA CREDIT MANAGER
WebUI.setText(findTestObject('Login/input_Silahkan Login_id-login'), findTestData('Credit Approval').getValue(4, 2))

WebUI.setText(findTestObject('Login/input_Silahkan Login_pass-login'), '3')

WebUI.click(findTestObject('Login/button_Login'))

WebUI.click(findTestObject('credit_approval/a_CREDIT APPROVAL PROCESS'))

//jika option enable approval 3
if (WebUI.verifyOptionPresentByLabel(findTestObject('credit_approval/pilih_job_approval'), '--SILAHKAN PILIH--', true, tunggu, 
    FailureHandling.OPTIONAL)) {
    WebUI.selectOptionByLabel(findTestObject('Object Repository/credit_approval/select_--SILAHKAN PILIH--165 - OPERATION DI_dd42a4'), 
        findTestData('Credit Approval').getValue(4, 1), true)
}

if (WebUI.verifyOptionNotPresentByLabel(findTestObject('credit_approval/pilih_cabang'), findTestData('Credit Approval').getValue(
        10, 1), true, tunggu, FailureHandling.OPTIONAL)) {
    WebUI.selectOptionByLabel(findTestObject('credit_approval/pilih_cabang'), findTestData('Credit Approval').getValue(9, 
            1), false)
}

WebUI.click(findTestObject('credit_approval/button_Find'))

WebUI.setText(findTestObject('credit_approval/input_Search_form-control input-sm'), findTestData('Import').getValue(2, 1))

if (WebUI.verifyElementPresent(findTestObject('credit_approval/i_Tidak_fa fa-pencil'), tunggu, FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('credit_approval/i_Tidak_fa fa-pencil'))

    WebUI.callTestCase(findTestCase('Credit Approval - Function - Click Tab'), [:])

    WebUI.selectOptionByLabel(findTestObject('credit_approval/select_--SILAHKAN PILIH--APPD-Approve  Docu_f606d3'), findTestData(
            'Credit Approval').getValue(3, 3), true)

    WebUI.callTestCase(findTestCase('Credit Approval - Function - Approval v2'), [:])

    WebUI.callTestCase(findTestCase('ACQ - Logout'), [:])
} else {
    WebUI.callTestCase(findTestCase('ACQ - Logout'), [:])
}

//Approval 4 - 176 - REGIONAL DIVISION HEAD
WebUI.setText(findTestObject('Login/input_Silahkan Login_id-login'), findTestData('Credit Approval').getValue(5, 2))

WebUI.setText(findTestObject('Login/input_Silahkan Login_pass-login'), '3')

WebUI.click(findTestObject('Login/button_Login'))

WebUI.click(findTestObject('credit_approval/a_CREDIT APPROVAL PROCESS'))

//jika option enable approval 4
if (WebUI.verifyOptionPresentByLabel(findTestObject('credit_approval/pilih_job_approval'), '--SILAHKAN PILIH--', true, tunggu, 
    FailureHandling.OPTIONAL)) {
    WebUI.selectOptionByLabel(findTestObject('Object Repository/credit_approval/select_--SILAHKAN PILIH--165 - OPERATION DI_dd42a4'), 
        findTestData('Credit Approval').getValue(5, 1), true)
}

if (WebUI.verifyOptionNotPresentByLabel(findTestObject('credit_approval/pilih_cabang'), findTestData('Credit Approval').getValue(
        10, 1), true, tunggu, FailureHandling.OPTIONAL)) {
    WebUI.selectOptionByLabel(findTestObject('credit_approval/pilih_cabang'), findTestData('Credit Approval').getValue(9, 
            1), false)
}

WebUI.click(findTestObject('credit_approval/button_Find'))

WebUI.setText(findTestObject('credit_approval/input_Search_form-control input-sm'), findTestData('Import').getValue(2, 1))

if (WebUI.verifyElementPresent(findTestObject('credit_approval/i_Tidak_fa fa-pencil'), tunggu, FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('credit_approval/i_Tidak_fa fa-pencil'))

    WebUI.callTestCase(findTestCase('Credit Approval - Function - Click Tab'), [:])

    WebUI.selectOptionByLabel(findTestObject('credit_approval/select_--SILAHKAN PILIH--APPD-Approve  Docu_f606d3'), findTestData(
            'Credit Approval').getValue(3, 3), true)

    WebUI.callTestCase(findTestCase('Credit Approval - Function - Approval v2'), [:])

    WebUI.closeBrowser()
	
	WebUI.callTestCase(findTestCase('Go To Link'), [:])
} else {
    WebUI.callTestCase(findTestCase('ACQ - Logout'), [:])
}

//Approval 5 - 123 - CREDIT OPERATION DIVISION HEAD
WebUI.setText(findTestObject('Login/input_Silahkan Login_id-login'), findTestData('Credit Approval').getValue(6, 2))

WebUI.setText(findTestObject('Login/input_Silahkan Login_pass-login'), '4')

WebUI.click(findTestObject('Login/button_Login'))

WebUI.click(findTestObject('credit_approval/a_CREDIT APPROVAL PROCESS'))

WebUI.delay(slow)

//jika option enable approval 5
if (WebUI.verifyOptionPresentByLabel(findTestObject('credit_approval/pilih_job_approval'), '--SILAHKAN PILIH--', true, tunggu, 
    FailureHandling.OPTIONAL)) {
    WebUI.selectOptionByLabel(findTestObject('Object Repository/credit_approval/select_--SILAHKAN PILIH--165 - OPERATION DI_dd42a4'), 
        findTestData('Credit Approval').getValue(6, 1), true)
}

if (WebUI.verifyOptionNotPresentByLabel(findTestObject('credit_approval/pilih_cabang'), findTestData('Credit Approval').getValue(
        10, 1), true, tunggu, FailureHandling.OPTIONAL)) {
    WebUI.selectOptionByLabel(findTestObject('credit_approval/pilih_cabang'), findTestData('Credit Approval').getValue(9, 
            1), false)
}

WebUI.click(findTestObject('credit_approval/button_Find'))

WebUI.setText(findTestObject('credit_approval/input_Search_form-control input-sm'), findTestData('Import').getValue(2, 1))

if (WebUI.verifyElementPresent(findTestObject('credit_approval/i_Tidak_fa fa-pencil'), tunggu, FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('credit_approval/i_Tidak_fa fa-pencil'))

    WebUI.callTestCase(findTestCase('Credit Approval - Function - Click Tab'), [:])

    WebUI.selectOptionByLabel(findTestObject('credit_approval/select_--SILAHKAN PILIH--APPD-Approve  Docu_f606d3'), findTestData(
            'Credit Approval').getValue(3, 3), true)

    WebUI.callTestCase(findTestCase('Credit Approval - Function - Approval v2'), [:])

    WebUI.callTestCase(findTestCase('ACQ - Logout'), [:])
} else {
    WebUI.callTestCase(findTestCase('ACQ - Logout'), [:])
}

//Approval 6 - 425 - REGIONAL OPERATION, CREDIT & COLLECTION SENIOR VICE PRESIDENT
WebUI.setText(findTestObject('Login/input_Silahkan Login_id-login'), findTestData('Credit Approval').getValue(7, 2))

WebUI.setText(findTestObject('Login/input_Silahkan Login_pass-login'), '3')

WebUI.click(findTestObject('Login/button_Login'))

WebUI.click(findTestObject('credit_approval/a_CREDIT APPROVAL PROCESS'))

//jika option enable approval 6
if (WebUI.verifyOptionPresentByLabel(findTestObject('credit_approval/pilih_job_approval'), '--SILAHKAN PILIH--', true, tunggu, 
    FailureHandling.OPTIONAL)) {
    WebUI.selectOptionByLabel(findTestObject('Object Repository/credit_approval/select_--SILAHKAN PILIH--165 - OPERATION DI_dd42a4'), 
        findTestData('Credit Approval').getValue(7, 1), true)
}

if (WebUI.verifyOptionNotPresentByLabel(findTestObject('credit_approval/pilih_cabang'), findTestData('Credit Approval').getValue(
        10, 1), true, tunggu, FailureHandling.OPTIONAL)) {
    WebUI.selectOptionByLabel(findTestObject('credit_approval/pilih_cabang'), findTestData('Credit Approval').getValue(9, 
            1), false)
}

WebUI.click(findTestObject('credit_approval/button_Find'))

WebUI.setText(findTestObject('credit_approval/input_Search_form-control input-sm'), findTestData('Import').getValue(2, 1))

if (WebUI.verifyElementPresent(findTestObject('credit_approval/i_Tidak_fa fa-pencil'), tunggu, FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('credit_approval/i_Tidak_fa fa-pencil'))

    WebUI.callTestCase(findTestCase('Credit Approval - Function - Click Tab'), [:])

    WebUI.selectOptionByLabel(findTestObject('credit_approval/select_--SILAHKAN PILIH--APPD-Approve  Docu_f606d3'), findTestData(
            'Credit Approval').getValue(3, 3), true)

    WebUI.callTestCase(findTestCase('Credit Approval - Function - Approval v2'), [:])

    WebUI.callTestCase(findTestCase('ACQ - Logout'), [:])
} else {
    WebUI.callTestCase(findTestCase('ACQ - Logout'), [:])
}

//Approval 7 - 165 - OPERATION DIRECTOR
WebUI.setText(findTestObject('Login/input_Silahkan Login_id-login'), findTestData('Credit Approval').getValue(8, 2))

WebUI.setText(findTestObject('Login/input_Silahkan Login_pass-login'), '3')

WebUI.click(findTestObject('Login/button_Login'))

WebUI.click(findTestObject('credit_approval/a_CREDIT APPROVAL PROCESS'))

//jika option enable approval 7
if (WebUI.verifyOptionPresentByLabel(findTestObject('credit_approval/pilih_job_approval'), '--SILAHKAN PILIH--', true, tunggu, 
    FailureHandling.OPTIONAL)) {
    WebUI.selectOptionByLabel(findTestObject('Object Repository/credit_approval/select_--SILAHKAN PILIH--165 - OPERATION DI_dd42a4'), 
        findTestData('Credit Approval').getValue(8, 1), true)
}

if (WebUI.verifyOptionNotPresentByLabel(findTestObject('credit_approval/pilih_cabang'), findTestData('Credit Approval').getValue(
        10, 1), true, tunggu, FailureHandling.OPTIONAL)) {
    WebUI.selectOptionByLabel(findTestObject('credit_approval/pilih_cabang'), findTestData('Credit Approval').getValue(9, 
            1), false)
}

WebUI.selectOptionByLabel(findTestObject('credit_approval/pilih_jenis_aplikasi'), findTestData('Credit Approval').getValue(
        11, 1), false)

WebUI.click(findTestObject('credit_approval/button_Find'))

WebUI.setText(findTestObject('credit_approval/input_Search_form-control input-sm'), findTestData('Import').getValue(2, 1))

if (WebUI.verifyElementPresent(findTestObject('credit_approval/i_Tidak_fa fa-pencil'), tunggu, FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('credit_approval/i_Tidak_fa fa-pencil'))

    WebUI.callTestCase(findTestCase('Credit Approval - Function - Click Tab'), [:])

    WebUI.selectOptionByLabel(findTestObject('credit_approval/select_--SILAHKAN PILIH--APPD-Approve  Docu_f606d3'), findTestData(
            'Credit Approval').getValue(3, 3), true)

    WebUI.callTestCase(findTestCase('Credit Approval - Function - Approval v2'), [:])

    WebUI.callTestCase(findTestCase('ACQ - Logout'), [:])
} else {
    WebUI.callTestCase(findTestCase('ACQ - Logout'), [:])
}

